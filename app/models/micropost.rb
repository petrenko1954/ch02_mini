#Листинг 2.12: Микросообщения принадлежат пользователю (micropost belongs to user). app/models/micropost.rb

class Micropost < ActiveRecord::Base
  belongs_to :user
  validates :content, length: { maximum: 140 }
end

